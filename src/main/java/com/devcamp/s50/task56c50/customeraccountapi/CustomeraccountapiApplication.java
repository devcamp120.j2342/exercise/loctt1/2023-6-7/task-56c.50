package com.devcamp.s50.task56c50.customeraccountapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeraccountapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeraccountapiApplication.class, args);
	}

}
