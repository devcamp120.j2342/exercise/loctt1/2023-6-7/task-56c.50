package com.devcamp.s50.task56c50.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c50.customeraccountapi.models.Customer;

@Service
public class CustomerService {
     private ArrayList<Customer> customers;

     public CustomerService(){
          customers = new ArrayList<>();
          customers.add(new Customer(1, "Tan Loc", 10000));
          customers.add(new Customer(2, "My Cam", 20000));
     }

     public ArrayList<Customer> getAlllCustomers(){
          return customers;
     }
}
