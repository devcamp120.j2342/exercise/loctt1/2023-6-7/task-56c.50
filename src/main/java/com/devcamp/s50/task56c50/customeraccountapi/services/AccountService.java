package com.devcamp.s50.task56c50.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c50.customeraccountapi.models.Account;
import com.devcamp.s50.task56c50.customeraccountapi.models.Customer;

@Service
public class AccountService {
     private ArrayList<Account> accounts;

     public AccountService(){
          CustomerService customerService = new CustomerService();
          ArrayList<Customer> customers = customerService.getAlllCustomers();

          accounts = new ArrayList<>();
          accounts.add(new Account(1, customers.get(0), 200.5));
          accounts.add(new Account(2, customers.get(1), 300.5));
     }

     public ArrayList<Account> getAllAccounts(){
          return accounts;
     }
}
