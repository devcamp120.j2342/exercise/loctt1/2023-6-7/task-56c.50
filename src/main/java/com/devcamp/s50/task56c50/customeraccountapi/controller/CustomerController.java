package com.devcamp.s50.task56c50.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c50.customeraccountapi.models.Customer;
import com.devcamp.s50.task56c50.customeraccountapi.services.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
     private CustomerService customerService;

     public CustomerController(CustomerService customerService){
          this.customerService = customerService;
     }

     @GetMapping("/customers")
     public ArrayList<Customer> getAllCustomers(){
          return customerService.getAlllCustomers();
     }
}
