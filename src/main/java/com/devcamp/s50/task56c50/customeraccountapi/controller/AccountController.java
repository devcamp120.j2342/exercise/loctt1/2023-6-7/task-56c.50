package com.devcamp.s50.task56c50.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c50.customeraccountapi.models.Account;
import com.devcamp.s50.task56c50.customeraccountapi.services.AccountService;

@RestController
@CrossOrigin
public class AccountController {
     private AccountService accountService;

     public AccountController(AccountService accountService){
          this.accountService = accountService;
     }

     @GetMapping("/accounts")
     public ArrayList<Account> getAllAccounts(){
          return accountService.getAllAccounts();
     }
}
